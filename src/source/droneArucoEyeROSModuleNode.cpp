//////////////////////////////////////////////////////
//  droneArucoEyeROSModuleNode.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on:
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O stream
//std::cout
#include <iostream>


//ROS
#include "ros/ros.h"

//Aruco Eye
#include "drone_aruco_eye_ros_module/droneArucoEyeROSModule.h"


#include "nodes_definition.h"


using namespace std;




int main(int argc,char **argv)
{
	//Ros Init
    ros::init(argc, argv, MODULE_NAME_ARUCO_EYE);
  	
    cout<<"[ROSNODE] Starting "<<ros::this_node::getName()<<endl;
  	
    DroneArucoEyeROSModule MyDroneArucoEye(argc, argv);
    MyDroneArucoEye.open();

    MyDroneArucoEye.run();
  	

    return 0;
}
